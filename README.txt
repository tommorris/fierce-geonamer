Fierce Geonamer is a small project to try and produce useful English
descriptions of locations.

The idea is you should be able to put in a lat and long and get back a useful
description like "London, United Kingdom" or "Orlando, Florida, United States"

There are geonaming services like Geonames and OpenStreetMap's Nominatim, but
they often provide either too much detail or too little for the use case of
providing a usable name for geolocating a blog post or photo.

Fierce Geonamer works like this:

a. take lat,long and send to Geonames.org
b. run the API response through an XSLT stylesheet, turn into intermediate XML
c. optionally turn intermediate XML into JSON

The choice of XSLT is to try and move the business logic into a
platform-independent processing layer. XSLT should be usable from most
languages and on most platforms: in Ruby using libxslt-ruby, in Python using
lxml, even in node.js using node_xsl (all using libxml/libxslt), in Java and
JVM platforms using SAXON, in .NET/Windows using MSXML, etc. etc.
