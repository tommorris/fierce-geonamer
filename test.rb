#!/usr/bin/env ruby
require 'test/unit'
require 'nokogiri'

class TestTransform < Test::Unit::TestCase
  def test_fixtures
    tests = `ls test/data/*.result.xml`.split("\n").map {|i| i.gsub(/test\/data\//, "") }.map {|i|
      [i.gsub(/result\./, ""), i]
    }
    longest_test_name = tests.map {|i| i[1] }.sort_by {|i| i.size }.last.size

    results = []

    tests.each do |test|
      input = test[0]
      expected_result = test[1]
      expected_result_parsed = Nokogiri::XML(File.open("test/data/#{expected_result}").readlines.join(""))

      actual_result_raw = `xsltproc src/transform.xsl test/data/#{input}`
      actual_result_parsed = Nokogiri::XML(actual_result_raw)

      # validation
      File.open("test/data/#{input}.transformed", 'w') {|i| i << actual_result_raw }
      actual_result_validates = system("xmllint --relaxng src/schema.rng test/data/#{input}.transformed > /dev/null")
      expected_result_validates = system("xmllint --relaxng src/schema.rng test/data/#{expected_result} > /dev/null")

      results << "#{input.ljust(longest_test_name)}\t #{actual_result_parsed.search("/geolocations/geolocation/humanReadableDescription").text}"

      assert(actual_result_parsed.canonicalize == expected_result_parsed.canonicalize, "actual result and expected result for #{input} do not match")
      assert(actual_result_validates, "actual result for #{input} does not validate")
      assert(expected_result_validates, "expected result for #{input} does not validate")
    end
    puts "\n"
    puts "Transformation results:"
    results.each {|line| puts line }
    system("rm test/data/*.transformed")
  end
end

