<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="geoname[./countryCode/text()='GB']">
        <geolocation>
            <humanReadableDescription>
                <xsl:choose>
                    <xsl:when test="toponymName/text() = 'London'">
                        <xsl:text>London</xsl:text>
                    </xsl:when>
					<xsl:when test="toponymName/text() = 'Brighton' and adminName2/text() = 'Brighton and Hove'">
						<xsl:text>Brighton, United Kingdom</xsl:text>
					</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="toponymName"/>
                        <xsl:choose>
                            <xsl:when test="adminName2/text() = 'Greater London'">
                                <xsl:text>, London</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>, </xsl:text>
                                <xsl:value-of select="adminName2"/>
                                <xsl:text>, </xsl:text>
                                <xsl:text>United Kingdom</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </humanReadableDescription>
            <hierarchy>
                <ol>
                <xsl:choose>
                    <xsl:when test="toponymName/text() = 'London'">
                        <li>
                          <xsl:text>London</xsl:text>
                        </li>
                    </xsl:when>
					<xsl:when test="toponymName/text() = 'Brighton' and adminName2/text() = 'Brighton and Hove'">
						<li>
							<xsl:text>Brighton</xsl:text>
						</li>
						<li>
							<xsl:text>United Kingdom</xsl:text>
						</li>
					</xsl:when>
                    <xsl:otherwise>
                        <li>
                            <xsl:value-of select="toponymName"/>
                        </li>
                        <xsl:choose>
                            <xsl:when test="adminName2/text() = 'Greater London'">
                                <li>London</li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:value-of select="adminName2"/>
                                </li>
                                <li>
                                    <xsl:text>United Kingdom</xsl:text>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                </ol>
            </hierarchy>
            <metadata>
                <xsl:if test="timezone">
                    <xsl:copy-of select="timezone"/>
                </xsl:if>
            </metadata>
        </geolocation>
    </xsl:template>
</xsl:stylesheet>
