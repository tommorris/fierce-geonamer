<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="geoname" priority="0">
        <geolocation>
            <humanReadableDescription>
                <xsl:value-of select="toponymName"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="adminName1"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="countryName"/>
            </humanReadableDescription>
            <hierarchy>
                <ol>
                    <li><xsl:value-of select="toponymName" /></li>
                    <li><xsl:value-of select="adminName1" /></li>
                    <li><xsl:value-of select="countryName"/></li>
                </ol>
            </hierarchy>
            <metadata>
                <xsl:if test="timezone">
                    <xsl:copy-of select="timezone" />
                </xsl:if>
            </metadata>
        </geolocation>
    </xsl:template>
</xsl:stylesheet>
