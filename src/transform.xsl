<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output indent="yes" method="xml" />
    <xsl:template match="/">
        <geolocations>
            <xsl:apply-templates />
        </geolocations>
    </xsl:template>
    
    <!-- Countries -->
    <xsl:include href="./country-us.xsl" />
    <xsl:include href="./country-il.xsl" />
    <xsl:include href="./country-gb.xsl" />
    <xsl:include href="./country-dk.xsl" />
    <xsl:include href="./country-nl.xsl" />
    <xsl:include href="./country-es.xsl" />
    <xsl:include href="./country-fallback.xsl" />
    
    <!-- XSL crap -->
    <xsl:template match="text()" />
</xsl:stylesheet>
