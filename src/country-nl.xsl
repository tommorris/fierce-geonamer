<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="geoname[./countryCode/text()='NL']">
        <geolocation>
            <humanReadableDescription>
                <xsl:value-of select="toponymName"/>
                <xsl:text>, </xsl:text>
                <xsl:text>Netherlands</xsl:text>
            </humanReadableDescription>
            <hierarchy>
                <ol>
                    <li><xsl:value-of select="toponymName" /></li>
                    <li><xsl:text>Netherlands</xsl:text></li>
                </ol>
            </hierarchy>
            <metadata>
                <xsl:if test="timezone">
                    <xsl:copy-of select="timezone" />
                </xsl:if>
            </metadata>
        </geolocation>
    </xsl:template>
</xsl:stylesheet>
